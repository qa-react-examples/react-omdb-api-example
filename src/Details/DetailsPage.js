import React from 'react';
import axios from 'axios';

export default class DetailsPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            film: {}
        };
    }

    componentDidMount() {
        axios.get('http://www.omdbapi.com/?apikey=PlzBanMe&i=' + this.props.match.params.id)
            .then(({ data }) => {
                this.setState({ film: data })
            });
    }

    render() {
        return (
            <div>
                <h1>{this.state.film.Title}</h1>
                <p>{this.state.film.Plot}</p>
            </div>
        )
    }
}