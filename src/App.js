import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import { BrowserRouter, Route, Switch } from 'react-router-dom';
import SearchPage from './Search/SearchPage';
import DetailsPage from './Details/DetailsPage';

function App() {
  return (
    <div className="App">
      <BrowserRouter>

      <Switch>
        <Route exact path="/" component={SearchPage} />
        <Route exact path="/details/:id" component={DetailsPage} />
      </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
