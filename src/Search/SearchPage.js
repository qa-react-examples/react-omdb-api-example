import React from 'react';
import axios from 'axios';
import ResultsList from './ResultsList';

export default class SearchPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            searchInput: '',
            results: []
        };
    }


    onSearchInputChange = (event) => {
        this.setState({ searchInput: event.target.value });
    };

    onSearchButtonClicked = () => {
        axios.get('http://www.omdbapi.com/?apikey=PlzBanMe&s=' + this.state.searchInput)
            .then(({ data }) => {
                this.setState({ results: data.Search || [] });
            });
    }

    onResultClicked = (event) => {
        this.props.history.push('/details/' + event);

    }

    render() {
        return (
            <div>
                <input value={this.state.searchInput} onChange={this.onSearchInputChange} />
                <button onClick={this.onSearchButtonClicked}>Search</button>

                <ResultsList results={this.state.results} onResultClick={this.onResultClicked} />

            </div>
        )
    }
}