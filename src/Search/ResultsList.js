import React from 'react';

const ResultsList = ({ results, onResultClick }) => (
    <div>
        {results.map(res => (
            <div key={res.imdbID} onClick={() => onResultClick(res.imdbID)}>
                {res.Title}
            </div>
        ))}
    </div>
)

export default ResultsList;